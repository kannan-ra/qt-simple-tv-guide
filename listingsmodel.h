#ifndef LISTINGSMODEL_H
#define LISTINGSMODEL_H

#include "apiconnector.h"
#include "listingitem.h"

#include <QAbstractListModel>
#include <QObject>

class ListingsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    ListingsModel(QObject *parent = 0);
    int rowCount(const QModelIndex& /*parent*/) const;
    QVariant data(const QModelIndex& index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    enum {PRGID=Qt::UserRole + 1,CHANNEL,TITLE,DESC,THUMBNAILURI,IMAGEURI,STARTTIME,ENDTIME,ACTORS,MOREINFO,MAX_COLS};

public slots:
    Q_INVOKABLE void getList(QString channel, QDate day);

private slots:
    void ListingsReceived(QList<ListingItem> listings);

private:
    ApiConnector theAtlas;
    QList<ListingItem> theListings;
};

#endif // LISTINGSMODEL_H
