#include "listingsmodel.h"

ListingsModel::ListingsModel(QObject *parent)
     : QAbstractListModel(parent)
 {
    QObject::connect(&theAtlas,
                     SIGNAL(ResponseReceived(QList<ListingItem>)),
                     this,
                     SLOT(ListingsReceived(QList<ListingItem>)));
    getList("cbbh", QDate::currentDate());
}


void ListingsModel::ListingsReceived(QList<ListingItem> resp)
{
    theListings.clear();
    beginInsertRows(QModelIndex(), 0, resp.size());
    foreach (ListingItem item, resp) {
        theListings.append(item);
    }
    endInsertRows();
}


void ListingsModel::getList(QString channel, QDate day = QDate::currentDate())
{
    theAtlas.CallApi(channel, day);
    theListings.clear();
}


int ListingsModel::rowCount(const QModelIndex& /*parent*/) const
{
    return theListings.size();
}

QVariant ListingsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= theListings.size())
        return QVariant();

    const ListingItem &listing = theListings[index.row()];
    switch(role)
    {
    case PRGID:
        return listing.prgId();
    case CHANNEL:
        return listing.channel();
    case TITLE:
        return listing.title();
    case DESC:
        return listing.desc();
    case THUMBNAILURI:
        return listing.thumbnailUri();
    case IMAGEURI:
        return listing.imageUri();
    case STARTTIME:
        return listing.startTime();
    case ENDTIME:
        return listing.endTime();
    case ACTORS:
        return listing.actors();
    case MOREINFO:
        return listing.moreInfoUri();
    default:
        qDebug() << "shouldn't be called!";
    }

    return QVariant();
}

QHash<int, QByteArray> ListingsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PRGID] = "PRGID";
    roles[CHANNEL] = "CHANNEL";
    roles[TITLE] = "TITLE";
    roles[DESC] = "DESC";
    roles[THUMBNAILURI] = "THUMBNAILURI";
    roles[IMAGEURI] = "IMAGEURI";
    roles[STARTTIME] = "STARTTIME";
    roles[ENDTIME] = "ENDTIME";
    roles[ACTORS] = "ACTORS";
    roles[MOREINFO] = "MOREINFO";
    return roles;
}
