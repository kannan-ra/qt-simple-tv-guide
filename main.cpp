#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "ListingsModel.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    ListingsModel *myListingsModel = new ListingsModel(0);
    engine.rootContext()->setContextProperty("myListingsModel", myListingsModel);
    engine.rootContext()->setContextProperty("_devpixelscale_", 1); //lenovo = 3.0917; // (20.105/2.835 = 7.0917)
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

/*
 * Desktop
qml: Screen: Width =  1680
qml: Screen: height =  1050
qml: Screen.pixelDensity (dots/mm):  2.8354450571988514
qml: Screen.logicalPixelDensity (dots/mm):  3.7795275590551185
qml: Screen.desktopAvailableWidth:  1680
qml: Screen.desktopAvailableHeight :  1020

Lenovo K900 Android
D/Qt      ( 9678): qrc:/main.qml:83 (onClicked): qml: Screen: Width =  1080
D/Qt      ( 9678): qrc:/main.qml:84 (onClicked): qml: Screen: height =  1920
D/Qt      ( 9678): qrc:/main.qml:85 (onClicked): qml: Screen.pixelDensity (dots/mm):  20.105263157894736
D/Qt      ( 9678): qrc:/main.qml:86 (onClicked): qml: Screen.logicalPixelDensity (dots/mm):  8.503937007874017
D/Qt      ( 9678): qrc:/main.qml:87 (onClicked): qml: Screen.desktopAvailableWidth:  1080
D/Qt      ( 9678): qrc:/main.qml:88 (onClicked): qml: Screen.desktopAvailableHeight :  1845
*/
