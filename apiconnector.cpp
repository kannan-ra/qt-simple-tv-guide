#include "apiconnector.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QList>

ApiConnector::ApiConnector(QObject *parent) :
    QObject(parent)
{
    pNam = new QNetworkAccessManager(this);
    QObject::connect(pNam,
                     SIGNAL(finished(QNetworkReply*)),
                     this,
                     SLOT(ProcessResponse(QNetworkReply*)));
}

void ApiConnector::CallApi(QString channel, QDate day)
{
    QUrl url(apiUrl);
    QDateTime fromTime = static_cast<QDateTime>(day);
    QDateTime toTime = fromTime.addDays(1);
    QString queryString = "channel_id=" + channel
            + "&publisher=bbc.co.uk"
            + "&annotations=channel,description,broadcasts,brand_summary"
            + "&from=" + fromTime.toString(Qt::ISODate)
            + "&to=" + toTime.toString(Qt::ISODate);
    url.setQuery(queryString);
    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json;charset=UTF-8");
    request.setHeader(QNetworkRequest::UserAgentHeader,"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");
    pNam->get(request);
}

void ApiConnector::ProcessResponse(QNetworkReply *apiReply)
{
    QString response = static_cast<QString>(apiReply->readAll());
    QList<ListingItem> listings;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(response.toUtf8());

    QJsonArray schedulesArray = jsonDoc.object()["schedule"].toArray();

    foreach (QJsonValue schedule, schedulesArray) {
        QJsonArray itemsArray = schedule.toObject()["items"].toArray();
        foreach (QJsonValue item, itemsArray) {
            QVariantMap containerMap = item.toObject()["container"].toObject().toVariantMap();
            if (!containerMap["title"].toString().isEmpty())
            {
                ListingItem listing;

                listing.setPrgId(containerMap["id"].toString());
                listing.setChannel(schedule.toObject()["channel_title"].toString());
                listing.setTitle(containerMap["title"].toString());
                listing.setDesc(containerMap["description"].toString());
                listing.setThumbnailUri(item.toObject()["thumbnail"].toString());
                listing.setImageUri(item.toObject()["image"].toString());
                QDateTime fromTxTime, toTxTime;
                fromTxTime = QDateTime::fromString(item.toObject()["broadcasts"].toArray()[0].toObject()["transmission_time"].toString(),Qt::ISODate);
                toTxTime = QDateTime::fromString(item.toObject()["broadcasts"].toArray()[0].toObject()["transmission_end_time"].toString(),Qt::ISODate);
                listing.setStartTime(fromTxTime);
                listing.setEndTime(toTxTime);
                listing.setMoreInfoUri(containerMap["uri"].toString());

                listings.append(listing);
            }
        }
    }
    emit(ResponseReceived(listings));
}

