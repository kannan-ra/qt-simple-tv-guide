#ifndef LISTINGITEM_H
#define LISTINGITEM_H

#include <QDateTime>
#include <QString>

class ListingItem
{
public:
    ListingItem();


    QString prgId() const;
    void setPrgId(const QString &prgId);

    QString channel() const;
    void setChannel(const QString &channel);

    QString title() const;
    void setTitle(const QString &title);

    QString desc() const;
    void setDesc(const QString &desc);

    QString thumbnailUri() const;
    void setThumbnailUri(const QString &thumbnailUri);

    QString imageUri() const;
    void setImageUri(const QString &imageUri);

    QDateTime startTime() const;
    void setStartTime(const QDateTime &startTime);

    QDateTime endTime() const;
    void setEndTime(const QDateTime &endTime);

    QString actors() const;
    void setActors(const QString &actors);

    QString moreInfoUri() const;
    void setMoreInfoUri(const QString &moreInfoUri);

private:
    QString mPrgId;
    QString mChannel;
    QString mTitle;
    QString mDesc;
    QString mThumbnailUri;
    QString mImageUri;
    QDateTime mStartTime;
    QDateTime mEndTime;
    QString mActors;
    QString mMoreInfoUri;
};

#endif // LISTINGITEM_H
