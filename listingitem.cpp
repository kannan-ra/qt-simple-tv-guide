#include "listingitem.h"

ListingItem::ListingItem() :
    mPrgId(""), mChannel(""), mTitle(""), mDesc(""),
    mThumbnailUri(""), mImageUri(""), mStartTime(QDateTime()), mEndTime(QDateTime()),
    mActors(""), mMoreInfoUri("")
{
}

QString ListingItem::prgId() const
{
    return mPrgId;
}

void ListingItem::setPrgId(const QString &prgId)
{
    mPrgId = prgId;
}

QString ListingItem::channel() const
{
    return mChannel;
}

void ListingItem::setChannel(const QString &channel)
{
    mChannel = channel;
}

QString ListingItem::title() const
{
    return mTitle;
}

void ListingItem::setTitle(const QString &title)
{
    mTitle = title;
}

QString ListingItem::desc() const
{
    return mDesc;
}

void ListingItem::setDesc(const QString &desc)
{
    mDesc = desc;
}

QString ListingItem::thumbnailUri() const
{
    return mThumbnailUri;
}

void ListingItem::setThumbnailUri(const QString &thumbnailUri)
{
    mThumbnailUri = thumbnailUri;
}

QString ListingItem::imageUri() const
{
    return mImageUri;
}

void ListingItem::setImageUri(const QString &imageUri)
{
    mImageUri = imageUri;
}

QDateTime ListingItem::startTime() const
{
    return mStartTime;
}

void ListingItem::setStartTime(const QDateTime &startTime)
{
    mStartTime = startTime;
}

QDateTime ListingItem::endTime() const
{
    return mEndTime;
}

void ListingItem::setEndTime(const QDateTime &endTime)
{
    mEndTime = endTime;
}

QString ListingItem::actors() const
{
    return mActors;
}

void ListingItem::setActors(const QString &actors)
{
    mActors = actors;
}

QString ListingItem::moreInfoUri() const
{
    return mMoreInfoUri;
}

void ListingItem::setMoreInfoUri(const QString &moreInfoUri)
{
    mMoreInfoUri = moreInfoUri;
}
