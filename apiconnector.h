#ifndef APICONNECTOR_H
#define APICONNECTOR_H

#include "listingitem.h"

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QList>

static QString apiUrl = "http://atlas.metabroadcast.com/3.0/schedule.json";

class ApiConnector : public QObject
{
    Q_OBJECT
public:
    explicit ApiConnector(QObject *parent = 0);
    void CallApi(QString channel, QDate day);

signals:
    void ResponseReceived(QList<ListingItem>);

public slots:
    void ProcessResponse(QNetworkReply* apiReply);

private:
    QNetworkAccessManager *pNam;

};

#endif // APICONNECTOR_H
