/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Quick Controls module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Window 2.1
import "content"

ApplicationWindow {
    visible: true
    width: 300 * _devpixelscale_
    height: 500 * _devpixelscale_

    Rectangle {
        color: "#212126"
        anchors.fill: parent
    }

    toolBar: BorderImage {
        border.bottom: 8 * _devpixelscale_
        source: "images/toolbar.png"
        width: parent.width
        height: 75 * _devpixelscale_

        Rectangle {
            id: backButton
            width: opacity ? (60 * _devpixelscale_) : 0
            anchors.left: parent.left
            anchors.leftMargin: 20 * _devpixelscale_
            opacity: stackView.depth > 1 ? 1 : 0
            anchors.verticalCenter: parent.verticalCenter
            antialiasing: true
            height: 50 * _devpixelscale_
            radius: 4 * _devpixelscale_
            color: backmouse.pressed ? "#222" : "transparent"
            Behavior on opacity { NumberAnimation{} }
            Image {
                anchors.verticalCenter: parent.verticalCenter
                source: "images/navigation_previous_item.png"
            }
            MouseArea {
                id: backmouse
                anchors.fill: parent
                anchors.margins: -10 * _devpixelscale_
                onClicked: {
                    //console.log("Screen: Width = ", Screen.width)
                    //console.log("Screen: height = ", Screen.height)
                    //console.log("Screen.pixelDensity (dots/mm): ", Screen.pixelDensity)
                    //console.log("Screen.logicalPixelDensity (dots/mm): ", Screen.logicalPixelDensity)
                    //console.log("Screen.desktopAvailableWidth: ", Screen.desktopAvailableWidth)
                    //console.log("Screen.desktopAvailableHeight : ", Screen.desktopAvailableHeight )
                    if (stackView.depth == 2){
                        apptitle.text = "TV Guide"
                    }
                    stackView.pop()
                }
            }
        }

        Text {
            id: apptitle
            font.pixelSize: 30 * _devpixelscale_
            Behavior on x { NumberAnimation{ easing.type: Easing.OutCubic} }
            x: backButton.x + backButton.width + (20 * _devpixelscale_)
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
            text: "TV Guide"
        }
    }

    ListModel {
        id: pageModel
        ListElement { title: "BBC One"; code: "cbbh"; chimage: "images/bbcone.png" }
        ListElement { title: "BBC Two"; code: "cbbG"; chimage: "images/bbctwo.png" }
        ListElement { title: "BBC Three"; code: "cbbP"; chimage: "images/bbcthree.png" }
        ListElement { title: "BBC Four"; code: "cbbQ"; chimage: "images/bbcfour.png" }
        ListElement { title: "CBBC"; code: "cbbW"; chimage: "images/bbccbbc.jpg" }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        // Implements back key navigation
        focus: true
        Keys.onReleased: if (event.key === Qt.Key_Back && stackView.depth > 1) {
                             stackView.pop();
                             event.accepted = true;
                         }

        initialItem: Item {
            width: parent.width
            height: parent.height
            ListView {
                model: pageModel
                anchors.fill: parent
                delegate: ChannelDelegate {
                    channeltitle: title
                    channelimage : chimage
                    onClicked:
                    {
                        apptitle.text = title
                        myListingsModel.getList(code, new Date())
                        stackView.push(Qt.resolvedUrl("content/ListItem.qml"),{channeltitle: title, channelimage: chimage})
                    }
                }
            }
        }
    }

}
