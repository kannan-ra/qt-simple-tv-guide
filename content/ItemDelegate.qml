import QtQuick 2.2

Item {
    id: tvListingItem
    width: parent.width
    height: 75 * _devpixelscale_

    property alias prgTitle: titleText.text
    property alias prgDesc: textitem1.text
    property alias prgThumbnailUri: thumbnailImage.source
    property alias prgImageUri: textitem2.text
    property alias prgStartTime: startTimeText.text
    property alias prgEndTime: textitem3.text
    property alias prgMoreInfoUri: textitem4.text

    Text { id: textitem1; visible: false }
    Text { id: textitem2; visible: false }
    Text { id: textitem3; visible: false }
    Text { id: textitem4; visible: false }

    signal clicked

    Rectangle {
        anchors.fill: parent
        color: "#11ffffff"
        visible: mouse.pressed
    }

    Image {
        id: thumbnailImage
        anchors.verticalCenter: parent.verticalCenter
        fillMode: Image.PreserveAspectFit
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: 100 * _devpixelscale_
        height: 75 * _devpixelscale_
    }

    Image {
        id: nextButton
        anchors.right: parent.right
        anchors.rightMargin: 10 * _devpixelscale_
        anchors.verticalCenter: parent.verticalCenter
        source: "../images/navigation_next_item.png"
    }

    Text {
        id: titleText
        color: "white"
        font.pixelSize: 15 * _devpixelscale_
        anchors.left: thumbnailImage.right
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.right: nextButton.left
        anchors.rightMargin: 10 * _devpixelscale_
        anchors.top: parent.top
        anchors.topMargin: 15 * _devpixelscale_
        elide: Text.ElideRight
        horizontalAlignment : Text.AlignRight
    }

    Text {
        id: startTimeText
        color: "white"
        font.pixelSize: 10 * _devpixelscale_
        anchors.right: nextButton.left
        anchors.rightMargin: 10 * _devpixelscale_
        anchors.top: titleText.bottom
        anchors.topMargin: 5 * _devpixelscale_
    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 15 * _devpixelscale_
        height: 1
        color: "#424246"
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: tvListingItem.clicked()
    }
}
