import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1

Item
{
    property string prgTitle_param
    property string prgDesc_param
    property string prgThumbnailUri_param
    property string prgImageUri_param
    property string prgStartTime_param
    property string prgEndTime_param
    property string prgMoreInfoUri_param

    Text {
        id: titleText
        text: prgTitle_param
        color: "white"
        font.pixelSize: 25 * _devpixelscale_
        anchors.left: parent.left
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.top: parent.top
        anchors.topMargin: 10 * _devpixelscale_
        elide: Text.ElideRight
        width: parent.width
    }


    Text {
        id: startTimeText
        text: prgStartTime_param
        color: "white"
        font.pixelSize: 10 * _devpixelscale_
        anchors.left: parent.left
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.top: titleText.bottom
        anchors.topMargin: 10 * _devpixelscale_
    }

    Text {
        id: endTimeText
        text: prgEndTime_param
        color: "white"
        font.pixelSize: 10 * _devpixelscale_
        anchors.left: startTimeText.right
        anchors.leftMargin: 10 * _devpixelscale_
        anchors.top: titleText.bottom
        anchors.topMargin: 10 * _devpixelscale_
    }

    Image {
        id: imagefield
        source: prgImageUri_param
        fillMode: Image.PreserveAspectFit
        anchors.left: parent.left
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.right: parent.right
        anchors.rightMargin: 5 * _devpixelscale_
        anchors.top: startTimeText.bottom
        anchors.topMargin: 20 * _devpixelscale_
        width: 320 * _devpixelscale_
        height: 180 * _devpixelscale_
    }

    Text {
        id: descText
        text: prgDesc_param
        color: "white"
        font.pixelSize: 15 * _devpixelscale_
        anchors.left: parent.left
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.top: imagefield.bottom
        anchors.topMargin: 10 * _devpixelscale_
        width: parent.width
        wrapMode: Text.WordWrap
    }
    Text {
        id: moreInfoUriText
        text: '<html><style type="text/css"></style><a href="'+prgMoreInfoUri_param+'" style="color: #3AABD5"><font color="#3AABD5">more info</a></html>'
        color: "white"
        font.pixelSize: 15 * _devpixelscale_
        anchors.left: parent.left
        anchors.leftMargin: 5 * _devpixelscale_
        anchors.top: descText.bottom
        anchors.topMargin: 10 * _devpixelscale_

        onLinkActivated: Qt.openUrlExternally(link)
    }

}
