import QtQuick 2.2

Item {
    id: channel
    width: parent.width
    height: 75 * _devpixelscale_

    property alias channeltitle: channelText.text
    property alias channelimage: channelimage.source

    signal clicked

    Rectangle {
        anchors.fill: parent
        color: "#11ffffff"
        visible: mouse.pressed
    }

    Image {
        id: channelimage
        fillMode: Image.PreserveAspectFit
        anchors.left: parent.left
        anchors.leftMargin: 15 * _devpixelscale_
        anchors.verticalCenter: parent.verticalCenter
        width: 80 * _devpixelscale_
        height: 60 * _devpixelscale_
    }


    Text {
        id: channelText
        color: "white"
        font.pixelSize: 15 * _devpixelscale_
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: channelimage.right
        anchors.leftMargin: 15 * _devpixelscale_
    }


    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 15 * _devpixelscale_
        height: 1
        color: "#424246"
    }

    Image {
        anchors.right: parent.right
        anchors.rightMargin: 20 * _devpixelscale_
        anchors.verticalCenter: parent.verticalCenter
        source: "../images/navigation_next_item.png"
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        onClicked: channel.clicked()

    }
}
