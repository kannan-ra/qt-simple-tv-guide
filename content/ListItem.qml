import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1

ScrollView {
    width: parent.width
    height: parent.height
    property string channeltitle
    property string channelimage
    flickableItem.interactive: true

    ListView {
        id: tvListings
        anchors.fill: parent
        model: myListingsModel

        delegate: ItemDelegate {
            prgTitle: TITLE
            prgDesc: DESC
            prgThumbnailUri: THUMBNAILURI
            prgImageUri: IMAGEURI
            prgStartTime: STARTTIME.toLocaleTimeString(Qt.locale("en_GB"), Locale.ShortFormat)
            prgEndTime: ENDTIME.toLocaleTimeString(Qt.locale("en_GB"), Locale.ShortFormat)
            prgMoreInfoUri: MOREINFO
            onClicked:
            {
                stackView.push(Qt.resolvedUrl("ListItemDetail.qml"),
                                {
                                   prgTitle_param: TITLE,
                                   prgDesc_param: DESC,
                                   prgThumbnailUri_param: THUMBNAILURI,
                                   prgImageUri_param: IMAGEURI,
                                   prgStartTime_param: STARTTIME.toLocaleTimeString(Qt.locale("en_GB"), Locale.ShortFormat),
                                   prgEndTime_param: ENDTIME.toLocaleTimeString(Qt.locale("en_GB"), Locale.ShortFormat),
                                   prgMoreInfoUri_param: MOREINFO
                                })
            }
        }
    }

    style: ScrollViewStyle {
        transientScrollBars: true
        handle: Item {
            implicitWidth: 14 * _devpixelscale_
            implicitHeight: 26 * _devpixelscale_
            Rectangle {
                color: "#3AABD5"
                anchors.fill: parent
                anchors.topMargin: 6 * _devpixelscale_
                anchors.leftMargin: 4 * _devpixelscale_
                anchors.rightMargin: 4 * _devpixelscale_
                anchors.bottomMargin: 6 * _devpixelscale_
            }
        }
        scrollBarBackground: Item {
            implicitWidth: 14 * _devpixelscale_
            implicitHeight: 26 * _devpixelscale_
        }
    }
}
