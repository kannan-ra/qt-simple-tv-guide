TEMPLATE = app

QT += qml quick widgets

SOURCES += main.cpp \
    apiconnector.cpp \
    listingsmodel.cpp \
    listingitem.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    apiconnector.h \
    listingsmodel.h \
    listingitem.h

OTHER_FILES += \
    main.qml \
    content/ItemDelegate.qml \
    content/ChannelDelegate.qml \
    content/ListItem.qml \
    content/ListItemDetail.qml
